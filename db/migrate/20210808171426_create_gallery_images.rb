class CreateGalleryImages < ActiveRecord::Migration[6.1]
  def change
    create_table :gallery_images do |t|
      t.string :title
      t.string :alt
      t.references :gallery
      t.timestamps
    end
  end
end
