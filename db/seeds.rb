# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

pp "=== Cleaning database ==="
User.destroy_all
Gallery.destroy_all
GalleryImage.destroy_all

pp "=== Creating users ==="
user_1 = FactoryBot.create(:user, email: 'user@test.com', password: 'secret123')
user_2 = FactoryBot.create(:user, email: 'user@example.com', password: 'secret123')

pp "=== Creating galleries ==="
gallery_1 = FactoryBot.create(:gallery, user: user_1, name: 'Test Gallery 1')
gallery_2 = FactoryBot.create(:gallery, user: user_1, name: 'Test Gallery 2')

pp "=== Creating images ==="
gallery_3 = FactoryBot.create(:gallery, user: user_2, name: 'Test Gallery 3')

5.times do
	FactoryBot.create(:gallery_image, gallery: gallery_2)
end

3.times do
	FactoryBot.create(:gallery_image, gallery: gallery_1)
end

2.times do
	FactoryBot.create(:gallery_image, gallery: gallery_3)
end