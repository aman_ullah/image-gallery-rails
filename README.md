# README

* Ruby version

3.0.0

* System dependencies

ImageMagick
sqlite

* Configuration

Install bundler
    gem install bundler
    
Install gems
    bundle install

Install npm packages
    yarn install

* Database creation

<!-- Drop existing db -->
    rails db:drop
<!-- Create fresh database -->
    rails db:create

* Database initialization

<!-- Migrate -->
    rails db:migrate
<!-- Seed database -->
    rails db:seed

* How to run the test suite

bundle exec rspec

* Start server

Compile assets
    bin/weback-dev-server

start server
    rails s