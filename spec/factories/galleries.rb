FactoryBot.define do
  factory :gallery do
    association :user
    sequence(:name) {|n| "Gallery #{n}" }
  end
end
