FactoryBot.define do
  factory :gallery_image do
    association :gallery
    sequence(:title) {|n| "Image #{n} title" }
    sequence(:alt) {|n| "Image #{n} alt" }
  end
end
