require 'rails_helper'

RSpec.describe GalleryImage, type: :model do
  describe 'Associations' do
    it { should belong_to(:gallery) }
  end

  describe 'Attachments' do
    it { should have_one_attached(:image) }
  end
end
