require 'rails_helper'

RSpec.describe Gallery, type: :model do
  describe 'associations' do
    it { should belong_to(:user) }
    it { should have_many(:gallery_images).dependent(:destroy) }
  end

  describe 'Validation' do
    it { should validate_presence_of(:name) }
  end
end
