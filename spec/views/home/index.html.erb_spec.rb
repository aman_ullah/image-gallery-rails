require 'rails_helper'

RSpec.describe 'home/index.html.erb', type: :view do
	it 'renders a link to view galleries' do
			render
			assert_select 'a[href=?]', galleries_path
	end
end