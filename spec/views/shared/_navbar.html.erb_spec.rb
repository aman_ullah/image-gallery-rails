require 'rails_helper'

RSpec.describe 'shared/_navbar.html.erb', type: :view do
  context 'when user is signed in' do
		before do
			user = FactoryBot.create(:user)
			sign_in user
		end

    it 'renders a link to log out' do
        render partial: 'shared/navbar'
				assert_select 'a[href=?]', destroy_user_session_path
    end
  end

	context 'when user is not signed in' do
    it 'renders a link to sign in' do
        render partial: 'shared/navbar'
				assert_select 'a[href=?]', new_user_session_path
    end

		it 'renders a link to register' do
			render partial: 'shared/navbar'
			assert_select 'a[href=?]', new_user_registration_path
		end
  end
end