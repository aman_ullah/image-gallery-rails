require 'rails_helper'

RSpec.describe 'users/users/show.html.erb', type: :view do
	let(:user) { FactoryBot.create(:user) }

	before do
		sign_in user
	end

	it 'renders a link to view all user galleries' do
		render
		assert_select 'a[href=?]', users_user_galleries_path(user)
	end
end