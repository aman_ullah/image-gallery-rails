require 'rails_helper'

RSpec.describe 'users/galleries/index.html.erb', type: :view do
	let(:user) { FactoryBot.create(:user) }

	before do
		sign_in user
		assign(:galleries, [])
	end

	it 'renders a link to create a new gallery' do
			render
			assert_select 'a[href=?]', new_users_user_gallery_path(user)
	end
end