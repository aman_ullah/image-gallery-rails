require 'rails_helper'

RSpec.describe 'users/galleries/_gallery.html.erb', type: :view do
	let(:user) { FactoryBot.create(:user, id: 1) }
	let(:gallery) { FactoryBot.build(:gallery, user: user, id: 1) }

	it 'renders a link to view the gallery' do
		render partial: 'users/galleries/gallery', locals: {gallery: gallery}
		assert_select 'a[href=?]', users_user_gallery_path(user, gallery), text: /View/
end

	it 'renders a link to edit the gallery' do
			render partial: 'users/galleries/gallery', locals: {gallery: gallery}
			assert_select 'a[href=?]', edit_users_user_gallery_path(user, gallery)
	end

	it 'renders a link to delete the gallery' do
		render partial: 'users/galleries/gallery', locals: {gallery: gallery}
		assert_select "a[href=?][data-method='delete']", users_user_gallery_path(user, gallery)
	end
end