require 'rails_helper'

RSpec.describe 'Home', type: :request do
  describe 'GET /' do
    it 'should return 200' do
        get root_path
        expect(response.status).to eq 200
    end
  end
end