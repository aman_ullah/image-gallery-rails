require 'rails_helper'

RSpec.describe 'User', type: :request do
	let(:user) { FactoryBot.create(:user) }

	describe 'GET /users/users/:user_id/galleries' do
		context 'when signed in' do
			before do
				sign_in user
			end

			it 'should return 200' do
				get users_user_galleries_path(user)
				expect(response.status).to eq 200
			end

			it 'renders a list pf user galleries' do
				FactoryBot.create(:gallery, user: user, name: 'User\'s First Gallery')
				FactoryBot.create(:gallery, user: user, name: 'User\'s Second Gallery')
				get users_user_galleries_path(user)
				assert_select 'p', text: 'User\'s First Gallery'
				assert_select 'p', text: 'User\'s Second Gallery'
			end

			it 'does not render galleries for other users' do
				other_user = FactoryBot.create(:user)
				FactoryBot.create(:gallery, user: other_user, name: 'Other\'s Gallery')
				get users_user_galleries_path(user)
				assert_select 'p', text: 'Other\'s Gallery', count: 0
			end
		end

		context 'when not signed in' do
			it 'should redirect to the sign in path' do
				get users_user_galleries_path(user)
				expect(response).to redirect_to new_user_session_path
			end
		end

		context 'when signed in as other user' do
			before do
				sign_in user
			end

			it 'should return a 401' do
				actual_user = FactoryBot.create(:user)
				get users_user_galleries_path(actual_user)
				expect(response.status).to eq 401
			end
		end
	end

	describe 'GET /users/users/:user_id/galleries/new' do
		before do
			sign_in user
		end

		it 'should return 200' do
			get new_users_user_gallery_path(user)
			expect(response.status).to eq 200
		end

		it "renders a form to create a gallery" do
			get new_users_user_gallery_path(user)
			assert_select 'form[action=?]', users_user_galleries_path(user)		
		end
	end

	describe 'GET /users/users/:user_id/galleries/:id/edit' do
		let(:gallery) { FactoryBot.create(:gallery, user: user) }
		
		before do
			sign_in user
		end

		it 'should return 200' do
			get edit_users_user_gallery_path(user, gallery)
			expect(response.status).to eq 200
		end

		it "renders a form to create a gallery" do
			get edit_users_user_gallery_path(user, gallery)
			assert_select 'form[action=?]', users_user_gallery_path(user, gallery)		
		end

		context 'when gallery belongs to other user' do
			let(:gallery) { FactoryBot.create(:gallery) }

			it 'returns a 404' do
				get edit_users_user_gallery_path(user, gallery)
				expect(response.status).to eq 404
			end
		end
	end

	describe 'POST /users/users/:user_id/galleries' do
		before do
			sign_in user
		end

		it 'should create a new gallery' do
			params = {gallery: {name: 'New Gallery'}}
			post users_user_galleries_path(user), params: params
			
			gallery = Gallery.last
			expect(gallery).to be
			expect(gallery.name).to eq 'New Gallery'
		end

		it 'sets a flash message' do
			params = {gallery: {name: 'New Gallery'}}
			post users_user_galleries_path(user), params: params
			
			expect(flash[:notice]).to eq 'Gallery created.'
		end

		it 'redirects to the index page' do
			params = {gallery: {name: 'New Gallery'}}
			post users_user_galleries_path(user), params: params
			
			expect(response).to redirect_to users_user_galleries_path(user)
		end
	end

	describe 'PATCH /users/users/:user_id/galleries/:id' do
		let(:gallery) { FactoryBot.create(:gallery, user: user, name: 'Old gallery name') }

		before do
			sign_in user
		end

		it 'should update the gallery' do
			params = {gallery: {name: 'New Gallery Name'}}
			patch users_user_gallery_path(user, gallery), params: params
			
			gallery.reload
			expect(gallery.name).to eq 'New Gallery Name'
		end

		it 'sets a flash message' do
			params = {gallery: {name: 'New Gallery'}}
			patch users_user_gallery_path(user, gallery), params: params
			
			expect(flash[:notice]).to eq 'Gallery updated.'
		end

		it 'redirects to the index page' do
			params = {gallery: {name: 'New Gallery'}}
			patch users_user_gallery_path(user, gallery), params: params
			
			expect(response).to redirect_to users_user_galleries_path(user)
		end
	end

	describe 'DELETE /users/users/:user_id/galleries/:id' do
		let(:gallery) { FactoryBot.create(:gallery, user: user) }

		before do
			sign_in user
		end

		it 'should delete the gallery' do
			delete users_user_gallery_path(user, gallery)
			
			expect(Gallery.any?).to be_falsey
		end

		it 'sets a flash message' do
			delete users_user_gallery_path(user, gallery)
			
			expect(flash[:notice]).to eq 'Gallery deleted.'
		end

		it 'redirects to the index page' do
			delete users_user_gallery_path(user, gallery)
			
			expect(response).to redirect_to users_user_galleries_path(user)
		end
	end
end