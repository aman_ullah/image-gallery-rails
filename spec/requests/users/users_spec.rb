require 'rails_helper'

RSpec.describe 'Users', type: :request do
	describe 'GET /users/users/:id' do
		let(:user) { FactoryBot.create(:user) }
		context 'when signed in' do
			before do
				sign_in user
			end

			it 'should return 200' do
				get users_user_path(user)
				expect(response.status).to eq 200
			end
		end

		context 'when not signed in' do
			it 'should redirect to the sign in path' do
				get users_user_path(user)
				expect(response).to redirect_to new_user_session_path
			end
		end

		context 'when signed in as other user' do
			before do
				sign_in user
			end

			it 'should return a 401' do
				actual_user = FactoryBot.create(:user)
				get users_user_path(actual_user)
				expect(response.status).to eq 401
			end
		end
	end
end