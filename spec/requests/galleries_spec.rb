require 'rails_helper'

RSpec.describe 'Home', type: :request do
  describe 'GET /galleries' do
    it 'should return 200' do
        get galleries_path
        expect(response.status).to eq 200
    end

		it 'renders all galleries' do
			FactoryBot.create(:gallery, name: 'Test Gallery')
			FactoryBot.create(:gallery, name: 'Example Gallery')
			get galleries_path
			expect(response.body).to include 'Test Gallery'
			expect(response.body).to include 'Example Gallery'
		end
  end
end