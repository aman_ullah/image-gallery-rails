module Users
  class UsersController < ApplicationController
		before_action :authenticate_user!
		before_action :validate_user

		def show
		end

		private

		def user_id
			params[:id]
		end

		def validate_user
			user = User.find_by(id: user_id)
			head 401 unless user.present? && user == current_user
		end
  end
end