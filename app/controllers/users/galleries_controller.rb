module Users
  class GalleriesController < UsersController
		before_action :set_gallery, only: [:edit, :update, :destroy, :show]

		def index
			@galleries = current_user.galleries
		end

		def show
		end

		def new
			@gallery = Gallery.new
		end

		def edit
		end

		def create
			@gallery = Gallery.new(gallery_params)
			@gallery.user = current_user
			if @gallery.save
				flash[:notice] = t('.gallery_created')
				redirect_to users_user_galleries_path(current_user)
			else
				render :new
			end
		end

		def update
			if @gallery.update(gallery_params)
				flash[:notice] = t('.gallery_updated')
				redirect_to users_user_galleries_path(current_user)
			else
				render :edit
			end
		end

		def destroy
			flash[:notice] = if @gallery.destroy
				 t('.gallery_deleted')
			else
				t('.failed_gallery_delete')
			end
			redirect_to users_user_galleries_path(current_user)
		end

		private

		def user_id
			params[:user_id]
		end

		def gallery_id
			params[:id]
		end

		def set_gallery
			@gallery = Gallery.find_by(id: gallery_id)
			head 404 unless @gallery.present? && @gallery.user == current_user
		end

		def gallery_params
			params.require(:gallery).permit(:name)
		end
  end
end