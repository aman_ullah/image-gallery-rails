module Users
  class GalleryImagesController < GalleriesController
		before_action :set_gallery
		before_action :set_gallery_image, only: [:edit, :update, :destroy]

		def new
			@image = GalleryImage.new
		end

		def edit
		end

		def create
			@image = GalleryImage.new(gallery_image_params)
			@image.gallery = @gallery
			if @image.save
				flash[:notice] = t('.image_created')
				redirect_to users_user_gallery_path(current_user, @gallery)
			else
				render :new
			end
		end

		def update
			if @image.update(gallery_image_params)
				flash[:notice] = t('.image_updated')
				redirect_to users_user_gallery_path(current_user, @gallery)
			else
				render :edit
			end
		end

		def destroy
			flash[:notice] = if @image.destroy
				 t('.image_deleted')
			else
				t('.failed_image_delete')
			end
			redirect_to users_user_gallery_path(current_user, @gallery)
		end

		private

		def user_id
			params[:user_id]
		end

		def gallery_id
			params[:gallery_id]
		end

		def gallery_image_id
			params[:id]
		end

		def set_gallery_image
			@image = GalleryImage.find_by(id: gallery_image_id)
			head 404 unless @image.present? && @image.gallery == @gallery
		end

		def gallery_image_params
			params.require(:gallery_image).permit(:title, :alt, :image)
		end
  end
end