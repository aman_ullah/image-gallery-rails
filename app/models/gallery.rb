class Gallery < ApplicationRecord
	# Associations
	belongs_to :user
	has_many :gallery_images, dependent: :destroy

	# Validations
	validates :name, presence: true
end
