Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  root 'home#index'

  resources :galleries, only: [:index, :show]

  namespace :users do
    resources :users, only: [:show] do
      resources :galleries do
        resources :gallery_images, except: [:index, :show]
      end
    end
  end
end
